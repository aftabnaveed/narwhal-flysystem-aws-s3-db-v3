<?php
namespace Narwhal\AwsS3DbV3;

use Aws\S3\S3Client;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Config;
use League\Glide\Responses\LaravelResponseFactory;
use Narwhal\AwsS3DbV3\Models\ImageCache;
use League\Glide\ServerFactory;

class AwsS3DbAdapter extends AwsS3Adapter
{
    /**
     * AwsS3DbAdapter constructor.
     * @param S3Client $client
     * @param string $bucket
     * @param string $prefix
     * @param array $options
     */
    public function __construct(S3Client $client, string $bucket, string $prefix = '', array $options = [])
    {
        parent::__construct($client, $bucket, $prefix, $options);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function has($path)
    {
        $exist = (new ImageCache)->wherePath($path)->first();
        return ($exist) ? true : false;
    }

    /**
     * @param string $path
     * @param string $contents
     * @param Config $config
     * @param array $dimensions
     * @return array|false|void
     */
    public function write($path, $contents, Config $config, $dimensions = [])
    {
        parent::write($path->getClientOriginalName(), $contents, $config);
        $image = ImageCache::create([
            'path' => $path->getClientOriginalName()
        ]);

        $storage = Storage::disk('s3')->getDriver();
        $server = ServerFactory::create([
            'response' => new LaravelResponseFactory(app('request')),
            'source' => $storage,
            'cache' => $storage,
            'source_path_prefix' => '/',
            'cache_path_prefix' => '/thumbnails',
            'group_cache_in_folders' => false,
            'cache_with_file_extensions' => true
        ]);

        $server->getImageResponse($image->path, $dimensions);
    }

    /**
     * @param string $path
     * @return bool|void
     */
    public function delete($path)
    {
        parent::delete($path);
        if (self::has($path)) {
            $image_cache = (new ImageCache)->wherePath($path)->first();
            $image_cache->delete();
        }
    }
}