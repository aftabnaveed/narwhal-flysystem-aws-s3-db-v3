<?php

namespace Narwhal\AwsS3DbV3;

use Illuminate\Support\ServiceProvider;

/**
 * Class AwsS3DbAdapterServiceProvider
 * @package Narwhal\AwsS3DbV3
 */
class AwsS3DbAdapterServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $migrationPath = __DIR__.'/../database/migrations';
        $this->publishes([
            $migrationPath => base_path('database/migrations')
        ], 'migrations');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}