<?php
namespace Narwhal\AwsS3DbV3\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ImageCache
 * @package Narwhal\AwsS3DbV3\Models
 * @property string path
 */
class ImageCache extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'path'
    ];
}